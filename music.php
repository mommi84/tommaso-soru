<?php
	$page = "Music";
	include "commons/header.php";
?>
	<div class="container" id="content">
		<div class="row">
			<div class="col-md-12 text-center">
				
				<h2>Music</h2>
				<h4>A few unsorted videos of myself drumming, live or in videoclips, between 2009 and today.</h4>
				
				<hr>
				
				<div style="position: relative; padding-bottom: 56.25%;">
					<iframe style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;" src="https://www.youtube.com/embed/videoseries?list=PLIAvziCRGGojLeIsjDynE1BApUm3fQskT&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
				</div>
				
<!--

Harddiskaunt 09–11 & 20–now
Hunted by Elephants 19–20
an envelope 16–19
D'Astray 15–17
The Hyra 13–15
molllust 12–15
Peaceful Sleep 09–13
theQueMMe 00–12
I Guastafeste 09–10
Collettivo 01 05–09
Ang'elica 02–03

-->

			</div>
		</div>
		<hr>
<?php
	include "commons/footer.php";
?>
</div><!-- end #content -->
</body>
</html>


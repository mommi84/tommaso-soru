<?php
	$page = "Mentoring";
	include "commons/header.php";
?>
	<div class="container" id="content">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				
				<h2>Academic Mentoring</h2>
			
				<div class="list-group">
					<a href="https://smilingprogrammer.github.io/GSoC-2024-blog/" target="_blank" class="list-group-item">
						<span class="glyphicon glyphicon-share" aria-hidden="true"></span>
						<b>Towards a Neural Extraction Framework / 4</b><br>
						Abdulsobur Oyewale<br>
						Google Summer of Code 2024</a>
					<a href="https://mehrzadshm.github.io/GSoC-2023-blog/" target="_blank" class="list-group-item">
						<span class="glyphicon glyphicon-share" aria-hidden="true"></span>
						<b>A Neural Question Answering Model for DBpedia / 9</b><br>
						Mehrzad Shahinmoghadam<br>
						Google Summer of Code 2023</a>
					<a href="https://sky-2002.github.io/" target="_blank" class="list-group-item">
						<span class="glyphicon glyphicon-share" aria-hidden="true"></span>
						<b>Towards a Neural Extraction Framework / 3</b><br>
						Aakash Thatte<br>
						Google Summer of Code 2023</a>
					<a href="https://sauravjoshi23.github.io/GSoC-Neural-QA-Model-DBpedia/" target="_blank" class="list-group-item">
						<span class="glyphicon glyphicon-share" aria-hidden="true"></span>
						<b>A Neural Question Answering Model for DBpedia / 8</b><br>
						Saurav Yogen Joshi<br>
						Google Summer of Code 2022</a>
					<a href="https://ananyaiitbhilai.github.io/DBpedia_GSoC2022_Neural_Extraction_Framework/" target="_blank" class="list-group-item">
						<span class="glyphicon glyphicon-share" aria-hidden="true"></span>
						<b>Towards a Neural Extraction Framework / 2</b><br>
						Ananya Hooda<br>
						Google Summer of Code 2022</a>
					<a href="https://imsiddhant07.github.io/Neural-QA-Model-for-DBpedia/" target="_blank" class="list-group-item">
						<span class="glyphicon glyphicon-share" aria-hidden="true"></span>
						<b>A Neural Question Answering Model for DBpedia / 7</b><br>
						Siddhant Jain<br>
						Google Summer of Code 2021</a>
					<a href="https://zoenantes.github.io/GSoc2021-DBpedia-NeuralExtraction/" target="_blank" class="list-group-item">
						<span class="glyphicon glyphicon-share" aria-hidden="true"></span>
						<b>Towards a Neural Extraction Framework / 1</b><br>
						Ziwei Xu<br>
						Google Summer of Code 2021</a>
					<a href="https://baiblanc.github.io/" target="_blank" class="list-group-item">
						<span class="glyphicon glyphicon-share" aria-hidden="true"></span>
						<b>A Neural Question Answering Model for DBpedia / 5</b><br>
						Zheyuan Bai<br>
						Google Summer of Code 2020</a>
					<a href="https://anandpanchbhai.com/A-Neural-QA-Model-for-DBpedia/" target="_blank" class="list-group-item">
						<span class="glyphicon glyphicon-share" aria-hidden="true"></span>
						<b>A Neural Question Answering Model for DBpedia / 3</b><br>
						Anand Panchbhai<br>
						Google Summer of Code 2019</a>
					<a href="http://bharatsuri.blogspot.com/" target="_blank" class="list-group-item">
						<span class="glyphicon glyphicon-share" aria-hidden="true"></span>
						<b>Knowledge Base Embeddings for DBpedia / 3</b><br>
						Bharat Suri<br>
						Google Summer of Code 2018</a>
					<a href="https://amanmehta-maniac.github.io" target="_blank" class="list-group-item">
						<span class="glyphicon glyphicon-share" aria-hidden="true"></span>
						<b>A Neural Question Answering Model for DBpedia / 1</b><br>
						Aman Mehta<br>
						Google Summer of Code 2018</a>
					<a href="https://github.com/dice-group/LINDA" target="_blank" class="list-group-item">
						<span class="glyphicon glyphicon-share" aria-hidden="true"></span>
						<b>Rule Mining on Distributed Data</b><br>
						Kunal Jha<br>
						Master's Thesis 2018, Uni Bonn &amp; Uni Leipzig</a>
					<a href="https://portal.imn.htwk-leipzig.de/termine/masterseminar-8-11.2017/" target="_blank" class="list-group-item">
						<span class="glyphicon glyphicon-share" aria-hidden="true"></span>
						<b><i>(Unofficial)</i> &#xDC;bersetzung von nat&#xFC;rlicher Sprache zu Sparql mittels Rekurrenter Neuronaler Netzwerke zur Anwendung in Question Answering Systemen &#xFC;ber Linked Open Data</b><br>
						Ann-Kathrin Hartmann<br>
						Master's Thesis 2018, HTWK Leipzig</a>
					<a href="http://nausheenfatma.wordpress.com/" target="_blank" class="list-group-item">
						<span class="glyphicon glyphicon-share" aria-hidden="true"></span>
						<b>Knowledge Base Embeddings for DBpedia / 2</b><br>
						Nausheen Fatma<br>
						Google Summer of Code 2017</a>
					<a href="http://akshayjagatap.wordpress.com/" target="_blank" class="list-group-item">
						<span class="glyphicon glyphicon-share" aria-hidden="true"></span>
						<b>Knowledge Base Embeddings for DBpedia / 1</b><br>
						Akshay Jagatap<br>
						Google Summer of Code 2017</a>
				</div>
				
			</div>
		</div>
		<hr>
<?php
	include "commons/footer.php";
?>
</div><!-- end #content -->
<script type="text/javascript">
// $( document ).ready(function() {
// 	$.ajax({
// 		url: "external/dblp.php",
// 	})
// 	.done(function( data ) {
// 		$("#dblp").html( data );
// 	});
// });
</script>
</body>
</html>


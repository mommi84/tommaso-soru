<?php
    $page = "Excuses To Eat";
    include "commons/header.php";
?>
    <div class="container" id="content">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                
                <h2>#ExcusesToEat2020 &mdash; Leaderboard</h2>

                <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col"></th>
                      <th scope="col">Country</th>
                      <th scope="col">Score</th>
                    </tr>
                  </thead>
                  <tbody id="dinners">
                  </tbody>
                </table>

            </div>
        </div>
        <hr>
<?php
    include "commons/footer.php";
?>
</div><!-- end #content -->
<script type="text/javascript">
 $( document ).ready(function() {
  $.ajax({
      url: "http://tommaso-soru.it:5000/apps/excusestoeat",
  })
  .done(function( data ) {
      var dinners = JSON.parse(data);
      console.log(dinners);
      $.each(dinners, function(i, dinner) {
        $("#dinners").append('<tr><th scope="row">'+ (i+1) + '.</th><td>' + dinner['flag'] + '</td><td>' + dinner['country'] + '</td><td>' + dinner['score'] + '</td></tr>')
      });
      // $("#data").html( dinners );
  });
 });
</script>
</body>
</html>


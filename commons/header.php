<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Tommaso Soru | data scientist, researcher, musician<?php echo " | $page"; ?></title>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-61325345-1', 'auto');
	  ga('send', 'pageview');

	</script>
	<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
	<style type="text/css">
	#content { margin-top: 60px; }
	.centered-text { text-align: center; }
	#heading h2 { padding-top: 40px; }
	</style>
	<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://code.jquery.com/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/bootstrap-slider.js"></script>
</head>
<body>

	<div class="navbar navbar-default navbar-fixed-top" role="navigation" id="header">
	  <div class="container">
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </button>
		  <a class="navbar-brand" href="./">Tommaso Soru</a>
		</div>
		<div class="navbar-collapse collapse">
			  <ul class="nav navbar-nav">
				<li><a class="align-text" href="./">CV</a></li>
				<li><a class="align-text" href="./publications">Publications</a></li>
				<li><a class="align-text" href="./research">Research</a></li>
				<li><a class="align-text" href="./mentoring">Mentoring</a></li>
				<li><a class="align-text" href="./portfolio">Portfolio</a></li>
				<li><a class="align-text" href="./music">Music</a></li>
			  </ul>
		</div><!--/.navbar-collapse -->
	  </div>
	</div>


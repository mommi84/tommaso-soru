<?php
	$page = "Portfolio";
	include "commons/header.php";
?>
	<div class="container" id="content">

		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<h2>Portfolio</h2>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2 col-md-offset-2 col-sm-2 col-sm-offset-2">
				<img src="images/tom-cupertino.jpg" style="max-width: 190px;" alt="" class="img-thumbnail img-responsive">
			</div>
			<div class="col-md-6 col-sm-6">
				<div class="list-group">
					<li class="list-group-item">
						<h4><a href="https://github.com/LiberAI">Liber AI</a> / <a href="https://github.com/LiberAI/NSpM">Neural SPARQL Machines</a></h4>
						<p>A Neural Machine Translation Approach to Question Answering over Knowledge Graphs.</p>
						<p><a href="https://arxiv.org/abs/1708.07624">Paper #1</a> &mdash;
						<a href="https://arxiv.org/abs/1806.10478">Paper #2</a> &mdash;
						<a href="https://github.com/LiberAI/NSpM">Code &amp; Data</a> &mdash;
						<a href="https://groups.google.com/forum/#!forum/neural-sparql-machines">Mailing List</a> &mdash;
						<a href="https://www.researchgate.net/project/Neural-SPARQL-Machines">ResearchGate</a> &mdash;
						<a href="https://twitter.com/theLiberAI">Twitter</a></p>
						<a role="button" class="btn btn-info btn-lg btn-block" href="https://medium.com/liber-ai" target="_blank"><span class="glyphicon glyphicon-share" aria-hidden="true"></span> Read Liber AI Blog</a>
					</li>
				</div>
				
			</div>
		</div>
		<hr>
<?php
	include "commons/footer.php";
?>
</div><!-- end #content -->
<script type="text/javascript">
</script>
</body>
</html>

<!-- https://en.cs.uni-paderborn.de/ds/news-single/rule-mining-and-inference-in-large-graphs -->
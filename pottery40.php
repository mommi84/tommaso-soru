<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pottery 40</title>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-61325345-1', 'auto');
      ga('send', 'pageview');

    </script>
    <style>
        body, html {
            margin: 0;
            padding: 0;
            height: 100%;
            width: 100%;
        }

        body {
            display: flex;
            align-items: center;
            justify-content: center;
            background-image: url('images/pottery40-engraved.jpg'); /* Replace with your image URL */
            background-size: cover;
            background-position: center;
            background-repeat: no-repeat;
            color: white;
            font-family: Arial, sans-serif;
            font-size: 2rem;
            text-align: center;
        }
    </style>
</head>
<body>
    <div>
        Pineapple Pizza Society out now on all major streaming platforms!
        <br><br>
        Spotify for Artists verification: Tommaso Soru (mommi84@gmail.com)
    </div>
</body>
</html>
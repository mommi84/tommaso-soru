<?php
	$page = "Publications";
	include "commons/header.php";
?>
	<div class="container" id="content">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				
				<h2>Publications</h2>

				<h4>To appear</h4>

<div class="well"><strong>Tommaso Soru</strong>, Jim Marshall, <a href="#">Anticipating the Future with Large Language Models.</a> In Proceedings, IEEE International Conference on Semantic Computing, 2025.</div>

				<h4>Published</h4>
				<div id="dblp">
					<div class="well">Loading publications from DBLP... <img src="images/loadingicon.gif" style="width: 20px;"></div>
				</div>

				<h4>Unindexed</h4>

<div class="well"><strong>Tommaso Soru</strong>, Stefano Ruberto, Diego Moussallem, Edgard Marx, <a href="http://groups.uni-paderborn.de/eim-i-fg-huellermeier/ecda2018/downloads/ECDA2018-BoA.pdf#page=93">A Simple and Fast Approach to Knowledge Graph Embeddings.</a> In Book of Abstracts, European Conference on Data Analysis, 2018.</div>
				
			</div>
		</div>
		<hr>
<?php
	include "commons/footer.php";
?>
</div><!-- end #content -->
<script type="text/javascript">
$( document ).ready(function() {
	$.ajax({
		url: "external/dblp.php",
	})
	.done(function( data ) {
		$("#dblp").html( data );
	});
});
</script>
</body>
</html>


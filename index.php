<?php
	$page = "Curriculum Vitae";
	include "commons/header.php";
?>
	<style type="text/css">
		.definitions {
			color: white;
			background: #333;
			padding: 2px 5px 2px 5px;
			border-radius: 5px;
		}
	</style>
	<div class="container" id="content">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 centered-text">
				<img src="images/tom.jpg" alt="tom" class="img-circle" width="300" height="300">
				<h2>Tommaso Soru</h2>
				<h4 style="color: #AAAAAA;">Preferred name: Tom</h4>
				<p><span class="definitions">Data Scientist</span>, <span class="definitions">Researcher</span>, <span class="definitions">Musician</span></p>
				<p>I'm an AI enthusiast with the dream of giving my small contribution to the quest for intelligence. My research topics include Entity Resolution, Link Prediction, and Question Answering over Knowledge Graphs, with a focus on the Semantic Web.</p>
				<p>
				<a role="button" class="btn btn-primary btn-lg btn-block" href="files/cv"><span class="glyphicon glyphicon-file" aria-hidden="true"></span> Download Curriculum Vitae</a>
				</p>
			</div>
		</div>
		<hr>
<?php
	include "commons/footer.php";
?>
</div><!-- end #content -->

</body>
</html>


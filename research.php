<?php
	$page = "Research";
	include "commons/header.php";
?>
	<div class="container" id="content">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				
				<h2>Research topics</h2>
			
				<div class="list-group">
					<a href="javascript:void(0);" class="list-group-item">AI-driven strategic foresight</a>
					<a href="javascript:void(0);" class="list-group-item">Link prediction</a>
					<a href="javascript:void(0);" class="list-group-item">Entity resolution</a>
					<a href="javascript:void(0);" class="list-group-item">Question answering</a>
					<a href="javascript:void(0);" class="list-group-item">Key discovery</a>
				</div>

				<h2>Supporting projects</h2>
				
				<div class="list-group">
					<a href="javascript:void(0);" class="list-group-item">DIESEL (2018)<br>Eurostars</a>
					<a href="javascript:void(0);" class="list-group-item">Linking LOD2 (2015-17)<br>German Research Foundation (DFG)</a>
					<a href="javascript:void(0);" class="list-group-item">Linking LOD (2012-15)<br>German Research Foundation (DFG)</a>
				</div>
				
				<h2>Reviews</h2>
				
				<div class="list-group">
					<a href="javascript:void(0);" class="list-group-item">(...)</a>
					<a href="javascript:void(0);" class="list-group-item">PC Member for KGSWC 2021<br>3rd Knowledge Graph and Semantic Web Conference</a>
					<a href="javascript:void(0);" class="list-group-item">Co-Chair for ICSC 2020 Resource Track<br>14th IEEE International Conference on Semantic Computing</a>
					<a href="javascript:void(0);" class="list-group-item">Co-Chair for ICSC 2019 Resource Track<br>13th IEEE International Conference on Semantic Computing</a>
					<a href="javascript:void(0);" class="list-group-item">Co-Chair for ICSC 2018 Resource Track<br>12th IEEE International Conference on Semantic Computing</a>
					<a href="javascript:void(0);" class="list-group-item">PC Member for SEMANTiCS 2018 posters<br>14th International Conference on Semantic Systems</a>
					<a href="javascript:void(0);" class="list-group-item">PC Member for CSCUBS 2018<br>The 5th Computer Science Conference for University of Bonn Students</a>
					<a href="javascript:void(0);" class="list-group-item">PC Member for ICTERI 2018 posters<br>14th International Conference on ICT in Education, Research, and Industrial Applications</a>
					<!-- <a href="javascript:void(0);" class="list-group-item">Reviewer for ?<br>International Journal of Engineering, 2017 issue</a> -->
					<a href="javascript:void(0);" class="list-group-item">Reviewer for ISWC 2017<br>16th International Semantic Web Conference</a>
					<a href="javascript:void(0);" class="list-group-item">PC Member for CSCUBS 2017<br>The 4th Computer Science Conference for University of Bonn Students</a>
					<a href="javascript:void(0);" class="list-group-item">PC Member for LDK 2017<br>Language, Data and Knowledge 2017</a>
					<a href="javascript:void(0);" class="list-group-item">Co-Chair for a special track at ICIW 2017<br>Knowledge Graph Construction and Consumption</a>
					<a href="javascript:void(0);" class="list-group-item">Reviewer for ESWC 2017<br>14th Extended Semantic Web Conference</a>
					<a href="javascript:void(0);" class="list-group-item">PC Member for CSCUBS 2016<br>The 3rd Computer Science Conference for University of Bonn Students</a>
					<a href="javascript:void(0);" class="list-group-item">Reviewer for WWW 2015<br>24th International World Wide Web Conference</a>
					<a href="javascript:void(0);" class="list-group-item">Reviewer for JWS<br>Journal of Web Semantics, 2015 issue</a>
					<a href="javascript:void(0);" class="list-group-item">PC Member for AISI 2015<br>The 1st International Conference On Advanced Intelligent System and Informatics</a>
					<a href="javascript:void(0);" class="list-group-item">Reviewer for AAAI-15<br>Twenty-Ninth AAAI Conference on Artificial Intelligence</a>
					<a href="javascript:void(0);" class="list-group-item">Reviewer for IJSWIS<br>International Journal on Semantic Web and Information Systems</a>
					<a href="javascript:void(0);" class="list-group-item">Reviewer for WWW 2013<br>22nd International World-Wide Web Conference</a>
				</div>
				
			</div>
		</div>
		<hr>
<?php
	include "commons/footer.php";
?>
</div><!-- end #content -->
<script type="text/javascript">
// $( document ).ready(function() {
// 	$.ajax({
// 		url: "external/dblp.php",
// 	})
// 	.done(function( data ) {
// 		$("#dblp").html( data );
// 	});
// });
</script>
</body>
</html>


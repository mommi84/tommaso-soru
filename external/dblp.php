<?php

				// articles to skip
				$toskip = array("http://arxiv.org/abs/1705.04380", "http://arxiv.org/abs/1708.07624");

				// $xmlstr = @file_get_contents("../ts.xml");
				$xmlstr = @file_get_contents("http://dblp.uni-trier.de/pers/xx/s/Soru:Tommaso.xml");
				$dblp = new SimpleXMLElement($xmlstr);
//				$n = $dblp["n"];
				foreach($dblp->r as $pub) {
					if($pub->inproceedings != "") {
						$elem = $pub->inproceedings;
						$type = "In proceedings";
					}
					if($pub->article != "") {
						$elem = $pub->article;
						$type = "Article";
					}
					if($elem->booktitle != "") {
						$venue = $elem->booktitle;
					}
					if($elem->journal != "") {
						$venue = $elem->journal;
					}
					$outstr = "";
					foreach($elem->author as $author) {
						// styling...
						if($author == "Tommaso Soru")
							$author = "<strong>$author</strong>";
						$outstr .= $author.", ";
					}
					// highlight certain venues...
					$addstyle = "";
					$comment = "";
					// skip duplicates
					if(in_array($elem->ee, $toskip)) {
						continue;
					}
					// WIMU @ ESWC 2018
					if($elem->ee == "https://doi.org/10.1007/978-3-319-93417-4_43") {
						$comment = "<h4>Best paper nominee &#127941;</h4>";
						$addstyle .= " style='border: 1px solid #888888; background: #EEEEEE;'";
					}
					// CostFed @ SEMANTiCS 2018
					if($elem->ee == "https://doi.org/10.1016/j.procs.2018.09.016") {
						$comment = "<h4>Best paper award &#127941;</h4>";
						$addstyle .= " style='border: 1px solid #888888; background: #EEEEEE;'";
					}
					// Catsear @ WSDM Cup 2017
					if($elem->ee == "http://arxiv.org/abs/1712.08352") {
						$comment = "<h4>4th place (top 12%) &#127941;</h4>";
						$addstyle .= " style='border: 1px solid #888888; background: #EEEEEE;'";
					}
					// Torpedo @ ICSC 2017
					if($elem->ee == "https://doi.org/10.1109/ICSC.2017.79") {
						$comment = "<h4>Best paper nominee &#127941;</h4>";
						$addstyle .= " style='border: 1px solid #888888; background: #EEEEEE;'";
					}
					// NSpM @ SEMANTiCS 2017
					if($elem->ee == "http://ceur-ws.org/Vol-2044/paper14/") {
						$comment = "<h4>Best poster nominee &#127941;</h4>";
						$addstyle .= " style='border: 1px solid #888888; background: #EEEEEE;'";
					}
					// ROCKER @ WWW 2015
					if($elem->ee == "https://doi.org/10.1145/2736277.2741642") {
						$comment = "<h4>14% acceptance rate &#127941;</h4>";
						$addstyle .= " style='border: 1px solid #888888; background: #EEEEEE;'";
					}
					$outstr .= "<a href='".$elem->ee."'>".$elem->title."</a> ";
					$outstr .= "$type, $venue, ".$elem->year.".";
					echo "<div class='well'$addstyle>$comment";
					echo $outstr;
					echo "</div>";
				}				
?>